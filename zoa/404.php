<?php
/**
 * 404
 *
 * @package zoa
 */

get_header();

get_template_part( 'template-parts/404' );

get_footer();
