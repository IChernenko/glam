<?php
/**
 * Theme functions file
 */

/**
 * Enqueue parent theme styles first
 * Replaces previous method using @import
 * <http://codex.wordpress.org/Child_Themes>
 */

add_action( 'wp_enqueue_scripts', 'zoa_enqueue_parent_theme_style', 99 );

function zoa_enqueue_parent_theme_style() {
	wp_enqueue_style( 'zoa-parent-style', get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'zoa-child-style', get_stylesheet_directory_uri() . '/style.css');
	
if ( is_rtl() ) 
   	wp_enqueue_style( 'zoa-rtl', get_stylesheet_directory_uri() . '/rtl.css');
}

/**SKU */
function sv_remove_product_page_skus( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }

    return $enabled;
}
add_filter( 'wc_product_sku_enabled', 'sv_remove_product_page_skus' );

/**Shopping cart -  Auto-Update */
add_action( 'wp_footer', 'cart_update_qty_script' );
function cart_update_qty_script() {
  if (is_cart()) :
   ?>
    <script>
        jQuery('div.woocommerce').on('change', '.qty', function(){
           jQuery("[name='update_cart']").removeAttr('disabled');
           jQuery("[name='update_cart']").trigger("click"); 
        });
   </script>
<?php
endif;
}

/**woo_checkout */

function savvy_disable_address_fields_validation( $address_fields_array ) {
 
	unset( $address_fields_array['postcode']['validate']);
	// you can also hook first_name and last_name, company, country, city, address_1 and address_2
 
	return $address_fields_array;
 
}
add_filter( 'woocommerce_default_address_fields' , 'savvy_disable_address_fields_validation' );



/**SVG */
function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');



//remove clear cart button
add_action( 'wp_loaded', function(){
    remove_action( 'woocommerce_cart_actions', 'zoa_clear_cart_url' );
} );